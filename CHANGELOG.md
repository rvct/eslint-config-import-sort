## [1.0.1](https://gitlab.com/rvct/eslint-config-import-sort/compare/v1.0.0...v1.0.1) (2022-01-22)


### Bug Fixes

* update installation instruction ([ab910b9](https://gitlab.com/rvct/eslint-config-import-sort/commit/ab910b9e14599ff4f26592dab146f6c1996836da))

# 1.0.0 (2022-01-20)


### Features

* add base rules for import sorting ([1d9b095](https://gitlab.com/rvct/eslint-config-import-sort/commit/1d9b095f350a597dda87d3178ed8ef02d5cff752))

# 1.0.0-alpha.1 (2022-01-19)


### Features

* add base rules for import sorting ([1d9b095](https://gitlab.com/rvct/eslint-config-import-sort/commit/1d9b095f350a597dda87d3178ed8ef02d5cff752))
