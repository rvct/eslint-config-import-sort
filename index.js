const fs = require("fs");
const path = require("path");

function applyDirFilter(file, base) {
  const isDirectory = fs.lstatSync(path.join(base, file)).isDirectory();
  // Ignore specific directory and directory started with specific symbols
  const isAllowedFolderName = /^(?![._]|node_modules)\w+/.test(file);

  return isDirectory && isAllowedFolderName;
}

function getDirs({ base = process.cwd(), depth = 0, dirs = [] } = {}) {
  const childDirs = fs
    .readdirSync(base)
    .filter((file) => applyDirFilter(file, base));

  return depth
    ? childDirs.reduce(
        (acc, dir) => [
          ...acc,
          ...getDirs({
            base: path.join(base, dir),
            depth: depth - 1,
            dirs: [dir],
          }),
        ],
        []
      )
    : [...dirs, ...childDirs];
}

module.exports = {
  parserOptions: {
    sourceType: "module",
  },
  extends: ["plugin:import/recommended"],
  plugins: ["simple-import-sort", "import"],
  settings: {
    "import/resolver": {
      node: {
        paths: getDirs(),
      },
    },
  },
  rules: {
    "import/no-unresolved": 0,
    "import/named": 0,
    "import/first": 2,
    "import/newline-after-import": 2,
    "import/no-duplicates": 2,
    "simple-import-sort/imports": [
      2,
      {
        groups: [
          // Node.js builtins.
          [
            "^('assert|assert/strict|async_hooks|buffer|child_process|cluster|console|constants|crypto|dgram|diagnostics_channel|dns|dns/promises|domain|events|fs|fs/promises|http|http2|https|inspector|module|net|os|path|path/posix|path/win32|perf_hooks|process|punycode|querystring|readline|repl|stream|stream/promises|string_decoder|sys|timers|timers/promises|tls|trace_events|tty|url|util|util/types|v8|vm|wasi|worker_threads|zlib')(/|$)",
          ],
          // Packages.
          ["^@?\\w"],
          // Side effect imports.
          ["^\\u0000"],
          // Internal packages.
          [`^(@|${getDirs({ depth: 1 }).join("|")})(/.*|$)`],
          // Parent imports. Put `..` last.
          ["^\\.\\.(?!/?$)", "^\\.\\./?$"],
          // Other relative imports. Put same-folder imports and `.` last.
          ["^\\./(?=.*/)(?!/?$)", "^\\.(?!/?$)", "^\\./?$"],
        ],
      },
    ],
    "simple-import-sort/exports": 2,
  },
};
