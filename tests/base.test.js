const process = require("process");

describe("Configuration", () => {
  const cwd = "/tmp";

  beforeEach(() => {
    jest.spyOn(process, "cwd").mockReturnValue(cwd);
    jest.resetModules();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it("should ignore files", () => {
    jest.mock("fs", () =>
      jest.requireActual("memfs").createFsFromVolume(
        jest.requireActual("memfs").Volume.fromNestedJSON(
          {
            file: "file",
          },
          "/tmp"
        )
      )
    );

    const config = require("../index");

    expect(config.settings["import/resolver"].node.paths).toEqual([]);
    expect(config.rules["simple-import-sort/imports"][1].groups[3]).toEqual([
      "^(@|)(/.*|$)",
    ]);
  });

  it("should ignore specific directories", () => {
    jest.mock("fs", () =>
      jest.requireActual("memfs").createFsFromVolume(
        jest.requireActual("memfs").Volume.fromNestedJSON(
          {
            file: "file",
            dir: {
              file: "file",
            },
            [".dir"]: {
              file: "file",
            },
            _dir: {
              file: "file",
            },
            node_modules: {
              file: "file",
            },
          },
          "/tmp"
        )
      )
    );
    const config = require("../index");

    expect(config.settings["import/resolver"].node.paths).toEqual(["dir"]);
    expect(config.rules["simple-import-sort/imports"][1].groups[3]).toEqual([
      "^(@|dir)(/.*|$)",
    ]);
  });

  it("should handle directories before specific depth", () => {
    jest.mock("fs", () =>
      jest.requireActual("memfs").createFsFromVolume(
        jest.requireActual("memfs").Volume.fromNestedJSON(
          {
            file: "file",
            dir1: {
              nested1: {
                deeplyNested1: {
                  file: "file",
                },
                file: "file",
              },
              file: "file",
            },
            dir2: {
              nested2: {
                file: "file",
              },
              file: "file",
            },
          },
          "/tmp"
        )
      )
    );

    const config = require("../index");

    expect(config.settings["import/resolver"].node.paths).toEqual([
      "dir1",
      "dir2",
    ]);
    expect(config.rules["simple-import-sort/imports"][1].groups[3]).toEqual([
      "^(@|dir1|nested1|dir2|nested2)(/.*|$)",
    ]);
  });
});
