# eslint-config-import-sort

Configuration set for easy autofixable import sorting based on
[eslint-plugin-simple-import-sort](https://github.com/lydell/eslint-plugin-simple-import-sort) and
[eslint-plugin-import](https://github.com/import-js/eslint-plugin-import) plugins

## Installation

Configration required installed plugins mentioned above

_npm_

```bash
npm install --save-dev eslint-config-import-sort eslint-plugin-simple-import-sort eslint-plugin-import
```

_yarn_

```bash
yarn add -D eslint-config-import-sort eslint-plugin-simple-import-sort eslint-plugin-import
```

_pnpm_

```bash
pnpm install -D eslint-config-import-sort eslint-plugin-simple-import-sort eslint-plugin-import
```

## Motivation

Javascript projects often use absolute imports based on the new of module bundlers and module syntax specs, these
require specific resolution, grouping and ordering linting rules.

It's not always obvious where `import x from 'module'` should look to find the file behind module.

Some plugins have specific rules for these purposes -
[import/resolver](https://github.com/import-js/eslint-plugin-import#resolvers) (eslint-plugin-import),
[simple-import-sort/imports, simple-import-sort/exports](https://github.com/lydell/eslint-plugin-simple-import-sort#sort-order)
(eslint-plugin-simple-import-sort).

## Examples

### Resolution rules

Let's imagine common javascript project

```bash
.
├── locales
├── node_modules
├── package.json
├── src
│   └── module1
├── tests
└── utils
    └── module2
```

Internal modules use absolute import in their implementation

```bash
import x1 from 'module1';
import x2 from 'module2';
```

In these cases resolvers are often used to avoid validation errors.

_.eslintrc.js_

```js
module.exports = {
  extends: ["plugin:import/recommended"],
  plugins: ["import"],
  settings: {
    "import/resolver": {
      node: {
        paths: ["src", "utils"],
      },
    },
  },
};
```

However, adding resolution paths is a routine task that varies from project to project. _eslint-config-import-sort_ adds
all directories from project root to settings.

### Ordering and grouping rules

_eslint-config-import-sort_ includes on
[eslint-plugin-simple-import-sort](https://github.com/lydell/eslint-plugin-simple-import-sort#sort-order) plugin with
high-customizable configuration.

eslint-config-import-sort takes on the routine task of creating the configuration for this plugin and reads a directory
structure to a depth of 1.

For structured represented below

```bash
.
├── locales
├── node_modules
├── package.json
├── src
│   ├── api
│   ├── middlewares
│   └── services
├── tests
└── utils
    ├── mappings
    └── url
```

_.eslintrc.js_ will be generated as

```js
module.exports = {
  plugins: ["simple-import-sort"],
  rules: {
    "simple-import-sort/imports": [
      2,
      {
        groups: [
          // List of the names of all modules provided by Node.js
          [
            "^('assert|assert/strict|async_hooks|buffer|child_process|cluster|console|constants|crypto|dgram|diagnostics_channel|dns|dns/promises|domain|events|fs|fs/promises|http|http2|https|inspector|module|net|os|path|path/posix|path/win32|perf_hooks|process|punycode|querystring|readline|repl|stream|stream/promises|string_decoder|sys|timers|timers/promises|tls|trace_events|tty|url|util|util/types|v8|vm|wasi|worker_threads|zlib')(/|$)",
          ],
          // Packages.
          ["^@?\\w"],
          // Side effect imports.
          ["^\\u0000"],
          // Internal packages.
          [`^(@|src|locales|tests|utils|mappings|url|api|middlewares|services)(/.*|$)`],
          // Parent imports. Put `..` last.
          ["^\\.\\.(?!/?$)", "^\\.\\./?$"],
          // Other relative imports. Put same-folder imports and `.` last.
          ["^\\./(?=.*/)(?!/?$)", "^\\.(?!/?$)", "^\\./?$"],
        ],
      },
    ],
  },
};
```

This allows to achieve sorting and grouping in modules imports without the need to follow the project structure.

### Excluded directories

_eslint-config-import-sort_ uses certain rules to create configurations, it ignores files, directories contains of
specific names and specific characters

```bash
.
├── .config /* ignored */
├── locales
├── node_modules /* ignored */
├── package.json /* ignored */
├── src
├── __tests__ /* ignored */
├── tests
└── utils
```
