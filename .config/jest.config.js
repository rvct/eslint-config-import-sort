module.exports = {
  rootDir: "../",
  moduleDirectories: ["<rootDir>", "node_modules"],
  testURL: "http://localhost/en/path",
  coverageDirectory: "<rootDir>/coverage",
  collectCoverageFrom: ["<rootDir>/index.js"],
  transformIgnorePatterns: ["node_modules"],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100,
    },
  },
};
