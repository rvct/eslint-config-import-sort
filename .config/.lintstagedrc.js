module.exports = {
  "*.{ts,tsx,js,jsx}": ["npm run test:lint -- --fix"],

  "*.{md,yml,yaml,json}": ["npm run test:prettier -- --write"],
};
